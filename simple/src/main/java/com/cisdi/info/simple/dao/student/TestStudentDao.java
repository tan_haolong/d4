package com.cisdi.info.simple.dao.student;

import com.cisdi.info.simple.dto.base.PageDTO;
import com.cisdi.info.simple.entity.student.TestStudent;
import org.apache.ibatis.annotations.Mapper;
import org.aspectj.weaver.ast.Test;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;
import java.util.List;

@Mapper
@Component(value = "testStudentDao")
public interface TestStudentDao {

    /**
    * 根据分页参数查询学生集合
    *
    * @param pageDTO 分页条件
    */
    public List<TestStudent> findTestStudents(PageDTO pageDTO);

    /**
    * 查询全部学生集合
    *
    */
    public List<TestStudent> findAllTestStudents();

    /**
    * 查询所有学生集合(只提取ID 和 Name)
    *
    */
    public List<TestStudent> findAllTestStudentsWithIdName();

    /**
    * 根据名称查询学生集合(只提取ID 和 Name)
    *
    * @param testStudentName 名称
    */
    public List<TestStudent> findTestStudentsWithIdNameByName(@Param("testStudentName") String testStudentName);

    /**
    * 根据ID查询指定的学生(只提取ID 和 Name)
    *
    * @param testStudentId Id
    */
    public TestStudent findTestStudentsWithIdNameById(@Param(" testStudentId") Long testStudentId);

    /**
    * 根据分页参数查询学生集合的数量
    *
    * @param pageDTO 分页条件
    */
    public Long findTestStudentTotalCount(PageDTO pageDTO);

    /**
    * 根据ID查询指定的学生
    *
    * @param testStudentId Id
    */
    public TestStudent findTestStudent(@Param("testStudentId") Long testStudentId);

    /**
    * 根据ID查询指定的学生(包含外键)
    *
    * @param testStudentId Id
    */
     public TestStudent findTestStudentWithForeignName(@Param("testStudentId") Long testStudentId);

    /**
    * 新增学生
    *
    * @param testStudent 实体对象
    */
    public Integer saveTestStudent(TestStudent testStudent);

    /**
    * 更新学生
    *
    * @param testStudent 实体对象
    */
    public Integer updateTestStudent(TestStudent testStudent);

    /**
    * 根据ID删除学生
    *
    * @param testStudentId ID
    */
    public Integer deleteTestStudent(@Param("testStudentId") Long testStudentId);

    public TestStudent findStudentById(Long studentId);
}
