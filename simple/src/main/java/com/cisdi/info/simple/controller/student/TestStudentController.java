

package com.cisdi.info.simple.controller.student;
import com.cisdi.info.simple.service.system.CodeTableService;
import com.cisdi.info.simple.dto.base.PageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import com.cisdi.info.simple.dto.base.PageResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cisdi.info.simple.dto.student.TestStudentEditDto;
import com.cisdi.info.simple.entity.student.TestStudent;
import com.cisdi.info.simple.service.student.TestStudentService;
import com.cisdi.info.simple.service.organization.OrganizationService;



/**module
{
"simple/student/TestStudent": {
"code": "simple/student/TestStudent",
"name1": "学生",
"url": "/simple/student/TestStudent",
"route": "/simple/student/TestStudent",
"iconClass": "",
"displayIndex": 1,
"parentCode": "simple/student",
"parentName": "学生管理",
"moduleType": "电脑模块",
"isInUse": "是",
"routeParamsObj": "",
"permissions":
	[
	{
	"code": "simple_student_TestStudent_Add",
	"name1": "新增学生",
	"fullName": "simple.学生管理.学生.新增",
	"moduleCode": "simple/student/TestStudent",
	urls:[
		"/simple/student/TestStudent/createTestStudent",
		"/simple/student/TestStudent/saveTestStudent"
	]
	},
	{
	"code": "simple_student_TestStudent_Edit",
	"name1": "编辑学生",
	"fullName": "simple.学生管理.学生.编辑",
	"moduleCode": "simple/student/TestStudent",
	urls:[
		"/simple/student/TestStudent/updateTestStudent"
	]
	},
	{
	"code": "simple_student_TestStudent_Delete",
	"name1": "删除学生",
	"fullName": "simple.学生管理.学生.删除",
	"moduleCode": "simple/student/TestStudent",
	urls:[
	"/simple/student/TestStudent/deleteTestStudent"
	]
	},
	{
	"code": "simple_student_TestStudent_View",
	"name1": "查看学生",
	"fullName": "simple.学生管理.学生.查看",
	"moduleCode": "simple/student/TestStudent",
	urls:[
	"/simple/student/TestStudent/findTestStudentForEdit",
	"/simple/student/TestStudent/findTestStudents",
	"/simple/student/TestStudent/findTestStudentForView"
		
,"/simple/organization/Organization/findOrganizationsWithIdNameByName"
	]
	}
	]
}
}
*/

@RestController
@RequestMapping("/simple/student/TestStudent")
@CrossOrigin(allowCredentials = "true")
public class TestStudentController {
private static Logger logger = LoggerFactory.getLogger(TestStudentController.class);



			
			
	@Autowired private TestStudentService testStudentService;
	@Autowired private CodeTableService codeTableService;
	@Autowired private OrganizationService organizationService;

	/**
	* 根据分页参数查询学生集合
	*
	* @param pageDTO 分页条件
	*/
	@PostMapping("/findTestStudents")
	public PageResultDTO findTestStudents(@RequestBody PageDTO pageDTO){
		return this.testStudentService.findTestStudents(pageDTO);
	}

	/**
	* 根据ID查询指定的学生
	*
	* @param testStudentId Id
	*/
	@GetMapping("/findTestStudent")
	public TestStudent findTestStudent(@RequestParam Long testStudentId){
		return this.testStudentService.findTestStudent(testStudentId);
	}

	/**
	* 根据ID查询指定的学生(包含外键名称)
	*
	* @param testStudentId Id
	*/
	@GetMapping("/findTestStudentForView")
	public TestStudent findTestStudentForView(@RequestParam Long testStudentId){
		return this.testStudentService.findTestStudentWithForeignName(testStudentId);
	}

	/**
	* 根据ID查询指定的学生(包含学生和外键名称)
	*
	* @param testStudentId Id
	*/
	@GetMapping("/findTestStudentForEdit")
	public TestStudentEditDto findTestStudentForEdit(@RequestParam Long testStudentId){
		TestStudentEditDto testStudentEditDto = new TestStudentEditDto();
		testStudentEditDto.setTestStudent(this.testStudentService.findTestStudentWithForeignName(testStudentId));

		this.prepareTestStudentEditDto(testStudentEditDto);

		return testStudentEditDto;
	}

	/**
	* 创建新的学生
	*
	*/
	@GetMapping("/createTestStudent")
	public TestStudentEditDto createTestStudent(){
		TestStudentEditDto testStudentEditDto = new TestStudentEditDto();
		testStudentEditDto.setTestStudent(new TestStudent());

		this.prepareTestStudentEditDto(testStudentEditDto);
		return testStudentEditDto;
	}

	private void prepareTestStudentEditDto(TestStudentEditDto testStudentEditDto){
		//testStudentEditDto.setGenderCodeTables(this.codeTableService.findCodeTablesByCodeType("Gender"));
        //TODO: 以下代码可以注释掉，此行代码即时加载所有外键对象，以便选择。如果不在此加载，可以在界面上实现延迟加载。如果外键对象超过 500 行，建议采用延迟加载
		testStudentEditDto.setOrganizationOrganizations(this.organizationService.findAllOrganizationsWithIdName());
	}

	/**
	* 更新学生
	*
	* @param testStudent 实体对象
	*/
	@PostMapping("/saveTestStudent")
	public TestStudent saveTestStudent(@RequestBody TestStudent testStudent){
		return this.testStudentService.saveTestStudent(testStudent);
	}

	@PostMapping("/updateTestStudent")
	public TestStudent updateTestStudent(@RequestBody TestStudent testStudent){
		return this.testStudentService.updateTestStudent(testStudent);
	}

	/**
	* 根据ID删除学生
	*
	* @param testStudentId ID
	*/
	@GetMapping("/deleteTestStudent")
	public void deleteTestStudent(@RequestParam Long testStudentId){
		this.testStudentService.deleteTestStudent(testStudentId);
	}

	/**
	* 根据ID查询指定的学生(只提取ID 和 Name)
	*
	* @param testStudentId Id
	*/
	@GetMapping("/findTestStudentsWithIdNameById")
	public TestStudent findTestStudentsWithIdNameById(@RequestParam Long testStudentId){
		return this.testStudentService.findTestStudentsWithIdNameById(testStudentId);
	}

	/**
	* 根据名称查询学生集合(只提取ID 和 Name)
	*
	* @param testStudentName 名称
	*/
	@GetMapping("/findTestStudentsWithIdNameByName")
	public List<TestStudent> findTestStudentsWithIdNameByName(String testStudentName){
		return this.testStudentService.findTestStudentsWithIdNameByName(testStudentName);
	}

	@GetMapping("/findStudentById")
	public TestStudent findTestStudentsWithIdNameByName(Long studentId){
		return this.testStudentService.findStudentById(studentId);
	}
}

