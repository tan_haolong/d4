package com.cisdi.info.simple.service.student;

import com.cisdi.info.simple.dto.base.PageResultDTO;
import com.cisdi.info.simple.entity.student.TestStudent;
import com.cisdi.info.simple.dto.base.PageDTO;

import java.util.List;

public interface TestStudentService {
    /**
     * 根据分页参数查询学生集合
     *
     * @param pageDTO 分页条件
     */
    public PageResultDTO findTestStudents(PageDTO pageDTO);

    /**
     * 查询全部学生集合
     *
     */
    public List<TestStudent> findAllTestStudents();

    /**
     * 根据名称查询学生集合(只提取ID 和 Name)
     *
     * @param testStudentName 名称
     */
    public List<TestStudent> findTestStudentsWithIdNameByName(String testStudentName);

    /**
     * 查询所有学生集合(只提取ID 和 Name)
     *
     */
    public List<TestStudent> findAllTestStudentsWithIdName();

    /**
     * 根据ID查询指定的学生(只提取ID 和 Name)
     *
     * @param testStudentId Id
     */
    public TestStudent findTestStudentsWithIdNameById(Long testStudentId);

    /**
     * 根据ID查询指定的学生
     *
     * @param testStudentId Id
     */
    public TestStudent findTestStudent(Long testStudentId);

    /**
     * 根据ID查询指定的学生(包含外键)
     *
     * @param testStudentId Id
     */
    public TestStudent findTestStudentWithForeignName(Long testStudentId);

    /**
     * 新增学生
     *
     * @param testStudent 实体对象
     */
    public TestStudent saveTestStudent(TestStudent testStudent);

    /**
     * 更新学生
     *
     * @param testStudent 实体对象
     */
     public TestStudent updateTestStudent(TestStudent testStudent);

    /**
     * 根据ID删除学生
     *
     * @param testStudentId ID
     */
     public void deleteTestStudent(Long testStudentId);

    /**
     * 根据ID找学生***/
    public TestStudent findStudentById(Long studentId);
}
