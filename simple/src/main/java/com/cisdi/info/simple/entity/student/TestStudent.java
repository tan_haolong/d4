package com.cisdi.info.simple.entity.student;

import com.cisdi.info.simple.entity.base.BaseEntity;
import javax.persistence.*;
import java.io.Serializable;
import com.cisdi.info.simple.annotation.DColumn;
import com.cisdi.info.simple.annotation.DEntity;

import java.util.Date;
import com.cisdi.info.simple.entity.organization.Organization;

@DEntity(label="学生",comment="",moduleLabel="学生管理")
@Entity(name="simple_test_student")
public class TestStudent extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@DColumn(index=3,label="学生姓名",comment="名称")
	@Column(name="student_name",length=20,nullable=false,unique=false)
	private String studentName;

	@DColumn(index=4,label="身份证号",comment="身份证号")
	@Column(name="student_id_number",length=255,nullable=false,unique=true)
	private String studentIdNumber;

	@DColumn(index=5,label="年龄",comment="年龄")
	@Column(name="student_age",length=255,nullable=true,unique=false)
	private Integer studentAge;

	@DColumn(index=6,label="入学时间",comment="入学时间")
	@Column(name="date_of_start_study",length=255,nullable=true,unique=false)
	private Date dateOfStartStudy;

	@DColumn(index=7,label="分数",comment="分数")
	@Column(name="score",length=255,nullable=true,unique=false)
	private Long score;

	@DColumn(index=8,label="单位",foreignEntity="Organization",comment="所属单位")
	@Column(name="organization_id",length=255,nullable=true,unique=false)
	private Long organizationId;

	@Transient
	private Organization organization;

	@Transient
	@DColumn(index=8,label="单位",foreignEntity="Organization",comment="所属单位")
	private String organizationName;

	@DColumn(index=9,label="性别",codeTable="Gender",comment="性别")
	@Column(name="gender",length=255,nullable=true,unique=false)
	private String gender;


	public String getStudentName() {
		return this.studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentIdNumber() {
		return this.studentIdNumber;
	}

	public void setStudentIdNumber(String studentIdNumber) {
		this.studentIdNumber = studentIdNumber;
	}

	public Integer getStudentAge() {
		return this.studentAge;
	}

	public void setStudentAge(Integer studentAge) {
		this.studentAge = studentAge;
	}

	public Date getDateOfStartStudy() {
		return this.dateOfStartStudy;
	}

	public void setDateOfStartStudy(Date dateOfStartStudy) {
		this.dateOfStartStudy = dateOfStartStudy;
	}

	public Long getScore() {
		return this.score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Long getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		if(organization == null){
			return;
		}
		this.organizationId = organization.getEId();
		this.organization = organization;
	}

	public String getOrganizationName() {
		return this.organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}


	
}