package com.cisdi.info.simple.dto.student;


import com.cisdi.info.simple.entity.student.TestStudent;
import com.cisdi.info.simple.entity.system.CodeTable;
import java.util.List;
import com.cisdi.info.simple.entity.organization.Organization;
public class TestStudentEditDto{

    private TestStudent testStudent;

    //码表是：Gender
    private List<CodeTable> genderCodeTables;

    //外键实体是：Organization
    private List<Organization> organizationOrganizations;


    public  TestStudent getTestStudent()
    {
        return this.testStudent;
    }
    public  void setTestStudent(TestStudent testStudent)
    {
        this.testStudent = testStudent;
    }

    public List<CodeTable> getGenderCodeTables()
    {
            return this.genderCodeTables;
    }
    public void setGenderCodeTables(List<CodeTable> genderCodeTables)
    {
        this.genderCodeTables = genderCodeTables;
    }

    public List<Organization> getOrganizationOrganizations()
    {
        return this.organizationOrganizations;
    }
    public void setOrganizationOrganizations(List<Organization> organizationOrganizations)
    {
        this.organizationOrganizations = organizationOrganizations;
    }
}
