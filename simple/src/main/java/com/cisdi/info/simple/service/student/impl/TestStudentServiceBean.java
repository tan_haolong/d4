package com.cisdi.info.simple.service.student.impl;

import com.cisdi.info.simple.dao.student.TestStudentDao;

import com.cisdi.info.simple.entity.base.BaseEntity;
import com.cisdi.info.simple.dto.base.PageDTO;
import com.cisdi.info.simple.dto.base.PageResultDTO;
import com.cisdi.info.simple.util.D4Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.cisdi.info.simple.entity.student.TestStudent;
import com.cisdi.info.simple.service.base.BaseService;
import com.cisdi.info.simple.service.student.TestStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Map;
import com.cisdi.info.simple.DDDException;

@Service
@Transactional
public class TestStudentServiceBean extends BaseService implements TestStudentService {

	//private static Logger logger = LoggerFactory.getLogger(TestStudentServiceBean.class);

	@Autowired
	private TestStudentDao testStudentDao;

	/**
	 * 根据分页参数查询学生集合
	 *
	 * @param pageDTO 分页条件
	 */
	@Override
	public PageResultDTO findTestStudents(PageDTO pageDTO){
        pageDTO.setStartIndex((pageDTO.getCurrentPage()-1)*pageDTO.getPageSize());
		List<TestStudent> testStudentDTOS = this.testStudentDao.findTestStudents(pageDTO);
		Long totalCount = this.testStudentDao.findTestStudentTotalCount(pageDTO);

		PageResultDTO pageResultDTO = new PageResultDTO();
		pageResultDTO.setTotalCount(totalCount);
		pageResultDTO.setDatas(testStudentDTOS);

		return pageResultDTO;
	}

	/**
	 * 查询全部学生集合
	 *
	 */
	@Override
	public List<TestStudent> findAllTestStudents(){
		return this.testStudentDao.findAllTestStudents();
	}

	/**
	 * 查询所有学生集合(只提取ID 和 Name)
	 *
	 */
	@Override
	public List<TestStudent> findAllTestStudentsWithIdName(){
		return this.testStudentDao.findAllTestStudentsWithIdName();
	}

	/**
	 * 根据名称查询学生集合(只提取ID 和 Name)
	 *
	 * @param testStudentName 名称
	 */
	@Override
	public List<TestStudent> findTestStudentsWithIdNameByName(String testStudentName){
		return this.testStudentDao.findTestStudentsWithIdNameByName(testStudentName);
	}

	/**
	 * 根据ID查询指定的学生(只提取ID 和 Name)
	 *
	 * @param testStudentId Id
	 */
	@Override
	public TestStudent findTestStudentsWithIdNameById(Long testStudentId){
		return this.testStudentDao.findTestStudentsWithIdNameById(testStudentId);
	}

	/**
	 * 根据ID查询指定的学生
	 *
	 * @param testStudentId Id
	 */
	@Override
	public TestStudent findTestStudent(Long testStudentId){
		return this.testStudentDao.findTestStudent(testStudentId);
	}

	/**
	 * 根据ID查询指定的学生(包含外键)
	 *
	 * @param testStudentId Id
	 */
	@Override
	public TestStudent findTestStudentWithForeignName(Long testStudentId){
		return this.testStudentDao.findTestStudentWithForeignName(testStudentId);
	}

	/**
	 * 新增学生
	 *
	 * @param testStudent 实体对象
	 */
	@Override
	public TestStudent saveTestStudent(TestStudent testStudent){
		//TODO:请在此校验参数的合法性
		this.setSavePulicColumns(testStudent);
		Integer rows = this.testStudentDao.saveTestStudent(testStudent);
		if(rows != 1)
		{
			String error = "新增保存学生出错，数据库应该返回1,但返回了 "+rows;
			throw new DDDException(error);
		}
		return testStudent;
	}

	/**
	 * 更新学生
	 *
	 * @param testStudent 实体对象
	 */
	@Override
	public TestStudent updateTestStudent(TestStudent testStudent){
		//TODO:请在此校验参数的合法性
		this.setUpdatePulicColumns(testStudent);
		Integer rows = this.testStudentDao.updateTestStudent(testStudent);
		if(rows != 1)
		{
			String error = "修改保存学生出错，数据库应该返回1,但返回了 "+rows+",数据可能被删除";
			throw new DDDException(error);
		}
		return testStudent;
	}

	/**
	 * 根据ID删除学生
	 *
	 * @param testStudentId ID
	 */
	@Override
	public void deleteTestStudent(Long testStudentId){
		Map<Class<? extends BaseEntity>,EntityUsage> entityUsageMap = this.checkForeignEntity(TestStudent.class, testStudentId);
		if(entityUsageMap != null && entityUsageMap.size() >0){
			StringBuilder errors = new StringBuilder();
			errors.append("计划删除的数据正在被以下数引用\n");
			for(EntityUsage entityUsage : entityUsageMap.values()){
				errors.append("\t").append(entityUsage.getEntityLabel()).append("\n");
				for(Map.Entry<Long,String> entry : entityUsage.getUsageIdNames().entrySet() ){
					errors.append("\t\t").append(entry.getKey()).append("\t").append(entry.getValue()).append("\n");
				}
			}
			errors.append("，不能删除，请检查处理后再删除");
			throw  new DDDException(errors.toString());
		}

		Integer rows = this.testStudentDao.deleteTestStudent(testStudentId);
		if(rows != 1){
			String error = "删除学生出错，数据可能已经被删除";
			throw new DDDException(error);
		}
	}

	@Override
	public TestStudent findStudentById(Long studentId)
	{
		return this.testStudentDao.findStudentById(studentId);
	}
}
